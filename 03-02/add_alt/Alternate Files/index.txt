<!--Add alt to Existing Images
The alt attribute is short for alternative text. As you may have seen on web pages that do not load properly, alt text appears to describe the image that is not rendering.
Do This
* Read the HTML and look at where images are used.
* Add a descriptive alt attribute to each image.-->

<!DOCTYPE html>
<html>
  <head>
  </head>
  <body>
    <h1>My Travels</h1>
    <h2>Kyoto, Japan</h2>
      <img src="Lanterns.jpg">
    <p>Photo by Nicholas Dench from FreeImages</p>
    <br>
    <h2>Paris, France</h2>
      <img src="Eiffel.jpg">
    <p>Photo by Benjamin Thorn from FreeImages</p>
    <br>
    <h2>Rio de Janeiro, Brazil</h2>
      <img src="Brazil.jpg">
    <p>Photo by Joe Carey from FreeImages</p>
  </body>
</html>
